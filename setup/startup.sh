#!/bin/sh

sleep 180

cp /mnt/temp/out/kubeconfig.yaml /opt/

export KUBECONFIG=/opt/kubeconfig.yaml

yq -i e '.clusters[0].cluster.server = "https://server:6443"' /opt/kubeconfig.yaml

kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.19.3/controller.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.6.1/aio/deploy/recommended.yaml

#kubeseal --fetch-cert
#kubeseal --format yaml --cert seal.pub < traefik/cousto/secret.yml >traefik/cousto/seal-secret.yml
#kubeseal -o yaml --secret-file d.yaml --sealed-secret-file d1.yaml --insecure-skip-tls-verify
#kubeseal -o yaml --secret-file /mnt/setup/secrets/argocd-initial-admin-secret.yaml --sealed-secret-file /mnt/setup/argocd/argocd-initial-admin-sealed-secret.yaml


#kubectl get secret default-token-m2gjc -o yaml -n kubernetes-dashboard | yq '.data.token' - | base64 -d

kubectl create namespace argocd || true
kubectl apply -k /mnt/setup/argocd

kubectl rollout status deployment argocd-server -n argocd
kubectl rollout status deployment kubernetes-dashboard -n kubernetes-dashboard

kubectl port-forward --address 0.0.0.0 svc/kubernetes-dashboard 9901:443 -n kubernetes-dashboard &
kubectl port-forward --address 0.0.0.0 svc/argocd-server        9902:443 -n argocd &

sleep infinity